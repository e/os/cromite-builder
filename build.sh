#!/bin/bash

set -e

chromium_dir="${chromium_dir:-/srv/chromium}"
root_dir=$(dirname "$(readlink -f "$0")")
if [ ! -d "$chromium_dir" ]; then
    chromium_dir=$root_dir
fi
chromium_version=$(head -n 1 "${root_dir}/cromite/build/RELEASE")
chromium_code=$(echo "$chromium_version" | tr -d '.' | cut -c5-)
clean=0
gsync=0
arch=""

usage() {
    echo "Usage:"
    echo "  $(basename $(readlink -nf $0)) [ options ]"
    echo
    echo "  Options:"
    echo "    -a <arch> Build specified arch"
    echo "    -c Clean"
    echo "    -h Show this message"
    echo "    -s Sync with history"
    echo
    exit 1
}

build() {
    echo ">> [$(date)] Head commit: $(git show -s --format=%s)"
    apks="ChromePublic"
    build_args="$(cat "${root_dir}"/cromite/build/bromite.gn_args) target_cpu=\"${1}\" "
    add_args=(
        "is_cfi=false"
        "use_thin_lto=false"
        "generate_linker_map=true"
        "use_relative_vtables_abi=false"
        "cc_wrapper=\"ccache\""
    )

    # Add to build_args
    for var in "${add_args[@]}"; do
        build_args+=" $var"
    done

    code=$chromium_code
    if [ $1 '==' "arm64" ]; then
        code+=50
    elif [ $1 '==' "x64" ]; then
        code+=60
    fi
    build_args+=' android_default_version_name="'$chromium_version'"'
    build_args+=' android_default_version_code="'$code'"'

    if [ $clean -eq 1 ] && [ -d "out/$1" ]; then
        rm -rf "out/$1"
    fi

    echo ">> [$(date)] Building chromium $chromium_version for $1"
    gn gen "out/$1" --args="$build_args"
    ninja -C out/$1 chrome_public_apk

    for apk in $apks; do
        if [ -f "out/${1}/apks/$apk.apk" ]; then
            echo ">> [$(date)] Moving $apk for ${1} to output folder"

            mv "out/${1}/apks/$apk.apk" "${root_dir}/apks/${apk}_${1}_v${chromium_version}.apk"
        fi
    done
}

setup_ccache() {
    echo ">> [$(date)] Settings up ccache"
    export USE_CCACHE=1
    export CCACHE_EXEC=$(command -v ccache)
    export PATH=$chromium_dir/src/third_party/llvm-build/Release+Asserts/bin:$PATH
    export CCACHE_CPP2=yes
    export CCACHE_SLOPPINESS=time_macros
    export CCACHE_DIR=$chromium_dir/.ccache
    ccache -M 200G
}

patch() {
    cd $chromium_dir/src

    echo ">> [$(date)] Remove v8 subrepo"
    rm -rf v8/.git
    git add -f v8 >/dev/null
    git commit -m ":NOEXPORT: v8 repo" >/dev/null

    echo ">> [$(date)] Applying bromite patches"

    patches_list=$(cat "${root_dir}/cromite/build/bromite_patches_list.txt")
    for file in $patches_list; do
        git am -3 --ignore-whitespace "${root_dir}/cromite/build/patches/$file"
    done
}

sync() {
    echo ">> [$(date)] Syncing chromium $chromium_version"
    cd $chromium_dir
    gclient sync -D --nohooks -r $chromium_version
    gclient runhooks
    patch
}

init_repo(){
    echo ">> [$(date)] Init chromium $chromium_version"
    cd $chromium_dir
    fetch --nohooks android
}

while getopts ":a:chr:s" opt; do
    case $opt in
    a) arch="$OPTARG" ;;
    c) clean=1 ;;
    h) usage ;;
    s) gsync=1 ;;
    :)
        echo "Option -$OPTARG requires an argument"
        echo
        usage
        ;;
    \?)
        echo "Invalid option:-$OPTARG"
        echo
        usage
        ;;
    esac
done
shift $((OPTIND - 1))

# Add depot_tools to PATH
if [ ! -d "$chromium_dir/depot_tools" ]; then
    git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git "$chromium_dir/depot_tools"
    clean=0
fi
export PATH="$chromium_dir/depot_tools:$PATH"

if [ $clean -eq 1 ]; then
    echo ">> [$(date)] Cleaning chromium source code"
    if [ -d "$chromium_dir/src" ]; then
        cd $chromium_dir/src
        git reset --hard && git clean -xfdf
    fi
fi

if [ ! -d "$chromium_dir/src" ]; then
    gsync=0
    init_repo
    sync
fi

if [ $gsync -eq 1 ]; then
    if [ -d "$chromium_dir/src/.git/rebase-apply" ]; then
        rm -fr "$chromium_dir/src/.git/rebase-apply"
    fi
    find "$chromium_dir/src/.git" -name "*.lock" -delete
    if [ ! -z $(find $chromium_dir/src/.git -name "*.lock") ]; then
        rm -fr $chromium_dir/src
        init_repo
    fi
    sync
fi

mkdir -p "${root_dir}/apks"

cd $chromium_dir/src
. build/android/envsetup.sh
setup_ccache

if [ ! -z "$arch" ]; then
    build $arch
else
    build arm64
    build x64
fi
